from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


class Camera:

    def __init__(self, x_eye, y_eye, z_eye, body_x, body_y, body_z):
        self.x_eye = x_eye
        self.y_eye = y_eye
        self.z_eye = z_eye
        self.body_x = body_x
        self.body_y = body_y
        self.body_z = body_z

    def moveCamera(self):
        # argNames=('eyeX', 'eyeY', 'eyeZ', 'centerX', 'centerY', 'centerZ', 'upX', 'upY', 'upZ')
        glMatrixMode(GL_MODELVIEW)
        # (left,right,bottom,top,zNear,zFar)
        glOrtho(-5, 5, -5, 5, -5, 10)
        gluLookAt(self.x_eye, self.y_eye, self.z_eye, self.body_x, self.body_y, self.body_z, 0, 1, 0)

    def cameraKeys(self, key, x, y):
        if key == b'w':
            self.body_z += 0.1
        if key == b's':
            self.body_z -= 0.1
        if key == b'a':
            self.body_x -= 0.1
        if key == b'd':
            self.body_x += 0.1
        if key == b'z':
            self.body_y += 0.1
        if key == b'x':
            self.body_y -= 0.1
        if key == b'\x1b':  # escape key...
            sys.exit()
        glutPostRedisplay()

    def cameraMouse(self, x, y):
        glutPostRedisplay()

    def cameraMouseWheel(self, wheel, direction, x, y):
        glutPostRedisplay()
