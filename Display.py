from OpenGL.GL import *
from OpenGL.GLUT import *

from Camera import Camera
from MapCube import MapCube


def preDraw(display):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glClearColor(0.5, 0.5, 1, 1)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LESS)
    display.camera.moveCamera()


class Display:

    def __init__(self):
        self.camera = Camera(0, 0, 2, 0, 0, 0)

    def showCube(self):
        preDraw(self)
        for x in range(1):
            MapCube(None).spawnCube(x * 1.5, 1, 1)

        glutSwapBuffers()
