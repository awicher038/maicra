import pygame
from OpenGL.GLUT import *

from Display import Display


class Main:

    def start(self):
        pygame.init()
        glutInit()  # Initialize a glut instance which will allow us to customize our window
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
        glutInitWindowSize(self.width, self.height)  # Set the width and height of your window
        glutInitWindowPosition(0, 0)  # Set the position at which this windows should appear
        window = glutCreateWindow("Test")  # Give your window a title

        display = Display()
        glutDisplayFunc(display.showCube)  # Tell OpenGL to call the showScren method continuously
        glutKeyboardFunc(display.camera.cameraKeys)
        glutSpecialFunc(display.camera.cameraKeys)
        # glutMotionFunc(display.camera.cameraMouse)
        # glutMouseWheelFunc(display.camera.cameraMouseWheel)
        glutMainLoop()  # Keeps the window created above displaying/running in a loop

    def __init__(self, width, height):
        self.width = width
        self.height = height
