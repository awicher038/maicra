import numpy
from TextureIndex import *
from OpenGL.GL import *
from PIL import Image as PImage


def loadTexture(img):
    raw = PImage.open("Textures/" + img + ".png")
    raw_data = numpy.array(list(raw.getdata()), numpy.int8)
    glEnable(GL_TEXTURE_2D)
    texID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texID)

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, raw.size[0], raw.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, raw_data)
    glGenerateMipmap(GL_TEXTURE_2D)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    # glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GL_FLOAT), GL_VOID_P)
    # glEnableVertexAttribArray(2)

    return texID


class MapCube:
    cube = (
        ((0.5, -0.5, -0.5), (0.5, 0.5, -0.5), (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5)),  # Front
        ((0.5, -0.5, 0.5), (0.5, 0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, -0.5, 0.5)),  # Back
        ((0.5, -0.5, 0.5), (0.5, 0.5, 0.5), (0.5, 0.5, -0.5), (0.5, -0.5, -0.5)),  # Right
        ((-0.5, -0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5)),  # Left
        ((0.5, 0.5, -0.5), (0.5, 0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, 0.5, -0.5)),  # Top
        ((0.5, -0.5, -0.5), (0.5, -0.5, 0.5), (-0.5, -0.5, 0.5), (-0.5, -0.5, -0.5))  # Bottom

    )
    textureCoord = ((0, 0), (1, 0), (1, 1), (0, 1))

    def spawnCube(self, offx, offy, offz):
        for f in MapCube.cube:
            glBegin(GL_POLYGON)
            for s, ct in zip(f, MapCube.textureCoord):
                glVertex3f(s[0] + offx, s[1] + offy, s[2] + offz)
                glTexCoord2f(ct[0], ct[1])
            glEnd()
            loadTexture(GRASS)

    def __init__(self, pngList):
        self.pngTextures = pngList
